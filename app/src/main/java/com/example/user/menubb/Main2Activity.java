package com.example.user.menubb;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Main2Activity extends AppCompatActivity {

    EditText ed2, ed3;
    Button b1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        ed2 = (EditText) findViewById(R.id.editText2);
        ed3 = (EditText) findViewById(R.id.editText3);


    }

    private boolean validasi() {
        boolean valid = true;
        String aa = ed2.getText().toString();
        String bb = ed3.getText().toString();

        if (aa.isEmpty() || !nomorbenar(aa)) {
            ed2.setError("Maaf Anda Salah Input");
            valid = false;
            return valid;

        }
        if (bb.isEmpty() || !nomorbenar(bb)) {
            ed3.setError("Maaf Anda Salah Input");
            valid = false;
            return valid;
        } else {
            return valid;
        }
    }

    public static Boolean nomorbenar(String nomorP) {
        Boolean valid = true;
        String rg = "(?:[0-9]+)";
        if (!nomorP.matches(rg)) {
            valid = false;
        }
        return valid;
    }

    public void hitung(View view) {
        if (validasi()) {

            Intent intent = new Intent(this, Menu_Sehat.class);

            Double d1 = Double.parseDouble(ed2.getText().toString());
            Double d2 = Double.parseDouble(ed3.getText().toString());
            Double hitung = d1 / 100;
            Double hitungnya = (d2 / (hitung * hitung));

            intent.putExtra("data", hitungnya);
            startActivity(intent);

        }
    }
}
