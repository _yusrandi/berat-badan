package com.example.user.menubb;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;

public class Login extends AppCompatActivity {

    EditText Nama, Pass;
    Dialog dialog;
    private String URL = urlConfig.URL_LOGIN;
    private String user, pass;
    Button buttonlogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

            Nama  =(EditText)findViewById(R.id.e3);
            Pass =(EditText)findViewById(R.id.editText);
            buttonlogin = (Button) findViewById(R.id.button2);

            buttonlogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    login();
                }
            });

    }

    void login(){
        user = Nama.getText().toString();
        pass = Pass.getText().toString();
        System.out.println(user +", "+pass);
        System.out.println("login klik");
        StringRequest request = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                System.out.println(response);
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    String code = jsonObject.getString("code");
                    String message = jsonObject.getString("message");

                    System.out.println(code);

                    if(code.equals("login_success")){

                        Intent intent = new Intent(Login.this, Pilihan.class);

                        startActivity(intent);

                    }else {

                        Toast.makeText(Login.this, "Data gagal Disimpan", Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    System.out.println(e);
                }

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println(error);
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Username", user);
                params.put("Password", pass);

                return params;
            }
        };
        MySingleton.getInstance(Login.this).addToRequestQueue(request);
    }

}

