package com.example.user.menubb;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.user.menubb.Model.hitung;
import com.example.user.menubb.bbadabter.bbadabter;

import java.util.ArrayList;

public class Pilihan extends AppCompatActivity {

    ListView li1;
    ArrayList<hitung> listView;
    bbadabter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pilihan);

        li1 = (ListView) findViewById (R.id.listView1);

        listView = new ArrayList<>();

        hitung h1 = new hitung();
        h1.setJenis("Perhitungan Berat Ideal Pria");
       // h1.setGambar(R.drawable.pint);

        listView.add(h1);

        hitung h2 = new hitung();
        h2.setJenis("Perhitungan Berat Ideal Wanita");
        //h2.setGambar();

        adapter = new bbadabter(this, R.layout.beratbadan, listView);

        li1.setAdapter(adapter);

        li1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    Intent intent = new Intent(Pilihan.this, Main2Activity.class);
                    startActivity(intent);
                }
                if (position == 1) {
                    Intent intent = new Intent(Pilihan.this, Menu_Utama.class);
                    startActivity(intent);
                }
            }
        });

    }
}
