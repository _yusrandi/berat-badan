package com.example.user.menubb.bbadabter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.user.menubb.Model.hitung;
import com.example.user.menubb.R;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by USER on 12/20/2017.
 */

public class bbadabter extends ArrayAdapter<hitung> {

    private ArrayList<hitung> list;

    public bbadabter(Context con, int textViewResourceId, ArrayList<hitung> list) {

        super(con, textViewResourceId, list);
        this.list = list;

    }
    public View getView(int pos, View converView, ViewGroup parent) {

        LayoutInflater li = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = li.inflate(R.layout.beratbadan, null);

        TextView jenis1 = (TextView) view.findViewById(R.id.jns);
        ImageView Gambar1 = (ImageView) view.findViewById(R.id.gbr);


        jenis1.setText(list.get(pos).getJenis());
        Gambar1.setImageResource(list.get(pos).getGambar());

        return view;

    }

}
