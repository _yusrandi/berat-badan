package com.example.user.menubb;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class Menu_Sehat extends AppCompatActivity {

    TextView tv1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu__sehat);

        tv1 = (TextView) findViewById(R.id.text);
        Intent intent = getIntent();

        Double hitung = intent.getDoubleExtra("data", 0);
        if (hitung < 18) {
            tv1.setText("Kurus");
        } else if (hitung > 18 && hitung < 23) {
            tv1.setText("Normal");
        } else if (hitung > 23 && hitung < 28) {
            tv1.setText("Gemuk");
        } else if (hitung > 28) {
            tv1.setText("Obesitas");
        }
    }
}
