package com.example.user.menubb;

import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Daftar extends AppCompatActivity {

    EditText Nama, Pass;
    RadioButton rbLaki, rbPr;
    Dialog dialog;
    private String URL = urlConfig.URL_REGISTER;
    private String jk, user, pass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar);

        Nama  =(EditText)findViewById(R.id.e);
        Pass =(EditText)findViewById(R.id.e2);
        rbLaki = (RadioButton) findViewById(R.id.rb_laki);
        rbPr = (RadioButton) findViewById(R.id.rb_pr);

    }
    public void Daftar(View view){
        if(rbLaki.isChecked()){
            jk = "Laki-Laki";
        }else if(rbPr.isChecked()){
            jk="Perempuan";
        }

        user = Nama.getText().toString();
        pass = Pass.getText().toString();

        daftar();

    }

    void daftar(){
        StringRequest request = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                System.out.println(response);
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    JSONObject jsonObject = jsonArray.getJSONObject(0);
                    String code = jsonObject.getString("code");
                    String message = jsonObject.getString("message");

                    System.out.println(code);

                    if(code.equals("reg_success")){

                        Intent intent = new Intent(Daftar.this, Login.class);
                        startActivity(intent);

                    }else {

                        Toast.makeText(Daftar.this, "Data gagal Disimpan", Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    System.out.println(e);
                }

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println(error);
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user", user);
                params.put("pass", pass);
                params.put("jk", jk);

                return params;
            }
        };
        MySingleton.getInstance(Daftar.this).addToRequestQueue(request);
    }

}
